module.exports = {
	CleanString(str){
		str = str.replace(/_/g, ' ');
		str = str.replace(/-/g, ' ');
		return str;
	},
	RetrieveFile(fileID,args){
		var client = args[0];
		var message = args[1];
		
		const hoot = message.client.commands.get('hoot');
		const { Attachment } = require('discord.js');
		if(!fileID){
			hoot.execute(message,"I'm having trouble finding that file right now...");
			return;
		}
		client.files.getDownloadURL(fileID.id)
		.then(downloadURL => {
			if(fileID.size > 8388608) {
				//the attachment is too big, send link instead
				hoot.execute(message,"This file is pretty big. Here's a link instead:");
				message.channel.send(downloadURL);
			} else {
				const attachment = new Attachment(downloadURL,fileID.name);

				//const attachment = new Attachment(stream.read(), 'test.png');
				// Send the attachment in the message channel with a content
				hoot.execute(message,"Fetching ``"+fileID.name+"``...");
				message.channel.send("",attachment);
			}

		})
		.catch(err => {console.log('Issue downloading file.', err); return;});
	}
	
};
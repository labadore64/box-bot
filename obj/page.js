module.exports = {
	index: 0,
	pages: 0,
	page_index: 0,
	current_choices: [],
	current_choice_input: [],
	AllResults: [],
	UpdatePage(start_index,max_page){
		this.index = start_index;
		this.pages = Math.floor((this.AllResults.length-1) / max_page)+1;
		this.page_index = Math.floor((this.index) / max_page)+1;
		
	},
	PagePrevious(max_page){
		this.index = this.index-max_page;
		if(this.index < 0){
			this.index = 0;
		}
		this.UpdatePage(this.index,max_page);
	},
	PageNext(max_page){
		this.index = this.index + max_page;
		if(this.index > this.AllResults.length-max_page-1){
			this.index = Math.floor((this.AllResults.length-1)/max_page)*max_page;
		}
		this.UpdatePage(this.index,max_page);
	},
	PageSet(newIndex,max_page){
		this.index = newIndex;
		if(this.index < 0){
			this.index = 0;
		}
		if(this.index > this.AllResults.length-max_page-1){
			this.index = Math.floor((this.AllResults.length-1)/max_page)*max_page;
		}
		this.UpdatePage(this.index,max_page);
	},
	ListString(string_message, quantity){
		const common = require('../script/common.js');
		this.current_choices = [];
		var limit = this.index+quantity;
		var counter = 0;
		
		for(var i = this.index; i < this.AllResults.length && i < limit; i++){
			string_message += (counter) + ") ``" + common.CleanString(this.AllResults[i].name) + "``\n";
			this.current_choices.push(this.AllResults[i]);
			counter++;
		}
		
		string_message += "\n``Total: " + this.AllResults.length + "``\n";
		return string_message;
	},
	//this creates the message that lets you update the page
	PageMessage(message,string_message,timeout,max_page,displayMethod,selectMethod,selectArgs){
		
		string_message = SetPageData(string_message,this.page_index,this.pages);
		
		const { Client, RichEmbed, Attachment } = require('discord.js');
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		current_choice_input = SetDefaultInputs();

		for(var i = 0; i < this.current_choices.length; i++){
			current_choice_input.push((i).toString());
		}
		
		const filter = response => {
			return current_choice_input.some(choice =>
			choice.toLowerCase() === response.content.toLowerCase().trim() &&
			message.author.id === response.author.id);
		};

		const embed = new RichEmbed()
		  .setTitle(nickname)
		  .setColor(embed_color)
		  .setThumbnail(img_avatar) 
		  .setDescription(string_message);
		  
		message.channel.send(embed).then(() => {
			message.channel.awaitMessages(filter, { maxMatches: 1, time: 12000, errors: ['time'] })
				.then(collected => {
				
					var answer = collected.first().content.trim();
					if(answer === 'previous' || 
						answer === 'prev'){
						this.PagePrevious(max_page);
						displayMethod();
					} else if (answer === 'next'){
						this.PageNext(max_page);
						displayMethod();
					} else if (answer === 'first'){
						this.PageSet(0,max_page);
						displayMethod();
					} else if (answer === 'last'){
						this.PageSet(10000,max_page);
						displayMethod();
					} else {
						//selected author
						//RetrieveFile(this.current_choices[answer],client);
						selectMethod(this.current_choices[answer],selectArgs);
						
					}
				})
				.catch(collected => {

				});
			}
		)
		
		function SetDefaultInputs(){
			var current_choice_input = [];
			current_choice_input.push("previous");
			current_choice_input.push("prev");
			current_choice_input.push("next");
			current_choice_input.push("first");
			current_choice_input.push("last");
			return current_choice_input;
		}
		
		function SetPageData(string_message,current,last){
			string_message += "\nPage " + current + "/"+ last + "\n\n";

			string_message += "To go to the next page, type ``next``.\n";
			string_message += "To go to the previous page, type ``previous``.\n";
			string_message += "To go to the first page, type ``first``.\n";
			string_message += "To go to the last page, type ``last``.";
			return string_message;
		}
	}
}
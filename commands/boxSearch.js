module.exports = {
	name: 'boxsearchdo',
	ignore:true,
	execute(message,args) {
		if(!args){
			args =[];
		}
		
		//gets hoot for messages.
		const common = require('../script/common.js');
		const max_page = 10;
		const hoot = message.client.commands.get('hoot');
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		var EventEmitter = require("events").EventEmitter;
		var search = new EventEmitter();
		var querySearch = "";
		// Extract the required classes from the discord.js module
		const { Attachment } = require('discord.js');

		// Import the native fs module
		const fs = require('fs');
		var pagebook = require('../obj/page.js');

		const { boxapp } = require('../config.json');

		var BoxSDK = require('box-node-sdk');


		var sdk = BoxSDK.getPreconfiguredInstance(boxapp);
		var client = sdk.getAppAuthClient('enterprise');

		var download_args = [];
		download_args[0] = client;
		download_args[1] = message;
		
		
		// Get your own user object from the Box API
		// All client methods return a promise that resolves to the results of the API call,
		// or rejects when an error occurs
		client.users.get(client.CURRENT_USER_ID)
			.then()
			.catch(err => {console.log('Could not login.', err); return;});

		SearchFile(args,client);
		
		//listeners
		
		search.on('update', function () {
			if(!search.data || search.data.total_count == 0){
				hoot.execute(message,"I couldn't find anything for ``"+querySearch+"``, sorry...");
				return;
			}
			if(search.data.total_count == 1){
				common.RetrieveFile(search.data.entries[0],download_args);
			} else {
				pagebook.AllResults = search.data.entries;
				pagebook.UpdatePage(0,max_page);
				search.data = {};
				AskFile();
			}
		});
		
		//end listeners
		
		
		function SearchFile(args,client){
			var searchString = 'test';
			if(args.length > 0){
				searchString = args[0];
			}
			
			client.search.query(
				searchString,
				{
					type: "file",
					fields: 'name,id,size',
					limit: 100,
					offset: 0,
					ancestor_folder_ids: "95797600433",
					query: searchString
				})
				.then(results => {
					search.data = results;
					querySearch=searchString;
					search.emit('update');
				}
			)

		}
		
		function AskFile(){
			var string_message = "Select which file for ``"+querySearch+"`` you want to retrieve.\n\n";
			string_message = pagebook.ListString(string_message,max_page);
			var args = [];
			args[0] = client;
			args[1] = message;
			pagebook.PageMessage(message,string_message,12000,max_page,AskFile,common.RetrieveFile,args);
		}
	}
};
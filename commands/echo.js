module.exports = {
	name: 'hoot',
	description: 'Repeats the text.',
	aliases: ['repeat','echo'],
	execute(message,text,returnMessage) {
	// Extract the required classes from the discord.js module
		const { Client, RichEmbed } = require('discord.js');
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		const embed = new RichEmbed()
		  .setTitle(nickname)
		  .setColor(embed_color)
		  .setThumbnail(img_avatar) 
		  .setDescription(text);
		message.channel.send(embed);
	}
};
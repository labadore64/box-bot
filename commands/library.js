module.exports = {
	name: 'library',
	description: 'Gets the link of the library.',
	aliases: ['lib'],
	execute(message,text) {
		//gets hoot for messages.
		const hoot = message.client.commands.get('hoot');
		
		var liblink = "https://app.box.com/folder/95797600433";
		
		hoot.execute(message,"Library URL:\n\n"+liblink);
	}
};
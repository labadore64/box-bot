module.exports = {
	name: 'author',
	description: 'List authors or author works.',
	execute(message,args) {
		//gets hoot for messages.
		const hoot = message.client.commands.get('hoot');
		
		const { Client, RichEmbed, Attachment } = require('discord.js');
		const fs = require('fs');

		const { boxapp } = require('../config.json');
		
		const common = require('../script/common.js');
		
		var BoxSDK = require('box-node-sdk');

		var sdk = BoxSDK.getPreconfiguredInstance(boxapp);
		var client = sdk.getAppAuthClient('enterprise');

		var EventEmitter = require("events").EventEmitter;
		
		var search = new EventEmitter();
		var searchTitles = new EventEmitter();
		
		var pagebook = require('../obj/page.js');
		
		const max_page = 10;

		// Get your own user object from the Box API
		// All client methods return a promise that resolves to the results of the API call,
		// or rejects when an error occurs
		client.users.get(client.CURRENT_USER_ID)
			.then()
			.catch(err => {console.log('Could not login.', err); return;});
		
		client.folders.getItems('95797600433',    
		{
			fields: 'name',
			offset: 0,
			limit: 1000
		})
		.then(results => 
			{
				search.data = results;
				search.emit('update');	
			}
		);
			
		//listeners
		
		search.on('update', function () {
			pagebook = require('../obj/page.js');
			if(!search.data || search.data.total_count == 0){
				hoot.execute(message,"I couldn't find anything, sorry...");
				return;
			}
			var results;
			
			if(args.length == 0){
				results = search.data.entries.filter(ent => ent.type === 'folder');
			} else if (args.length == 1){
				results = search.data.entries.filter(ent => ent.type === 'folder'
														&& ent.name.toLowerCase()
														.includes(args[0].toLowerCase()));
			}
			
			if(results.length == 0){
				hoot.execute(message,"I couldn't find anything, sorry...");
				return;	
			}
			pagebook.AllResults = results;
			
			results = [];
			
			if(pagebook.AllResults.length == 1){
				DisplayBooksOfAuthor(pagebook.AllResults[0]);
			} else {
				UpdatePage(pagebook.index);
				DisplayAuthors();
			}
			
		});
		
		searchTitles.on('update', function () {
			pagebook = require('../obj/page.js');
			if(!search.data || search.data.total_count == 0){
				hoot.execute(message,"I couldn't find anything, sorry...");
				return;
			}
			var results;
			
			results = searchTitles.data.entries.filter(ent => ent.type === 'file' 
														&& !ent.name.toLowerCase()
														.includes(".boxnote"));
			
			if(results.length == 0){
				hoot.execute(message,"I couldn't find anything, sorry...");
				return;	
			}
			pagebook.AllResults = results;
			results = [];
			UpdatePage(pagebook.index);
			DisplayBooks();
			
		});
		
		//end listeners
		
		//functions
		function DisplayBooksOfAuthor(folder){
			
			pagebook.index = 0;
			pagebook.pages = 0;
			pagebook.page_index = 0;
			
			client.folders.getItems(folder.id,    
			{
				fields: 'name,size',
				offset: 0,
				limit: 1000
			}).then(results => 
			{
				searchTitles.data = results;
				searchTitles.emit('update');	
			}
			);
		}
		
		function DisplayBooks(){
			var display = "To download the book, input the number.\n\n";
			
			display = pagebook.ListString(display,max_page);
			
			PageBooks(display);
		}
		
		function PageBooks(string_message){
			var args = [];
			args[0] = client;
			args[1] = message;
			pagebook.PageMessage(message,string_message,12000,max_page,DisplayBooks,common.RetrieveFile,args);
		}
		
		function DisplayAuthors(){
			var display = "To search for works under an author, input the number.\n\n";
			
			display = pagebook.ListString(display,max_page);

			PageAuthors(display);
		}
		
		function PageAuthors(string_message){
			pagebook.PageMessage(message,string_message,12000,max_page,DisplayAuthors,DisplayBooksOfAuthor);
		}
		
		function ListString(string_message,start_index, quantity){
			return common.ListString(string_message,pagebook.AllResults,start_index,quantity);
		}
		
		function UpdatePage(start_index){
			pagebook.UpdatePage(start_index,max_page);
		}
		//end functions
	}
};
module.exports = {
	name: 'search',
	description: 'Searches for a PDF.',
	aliases: ['search,lookup,find'],
	cooldown: 10,
	execute(message,args) {

		var boxSearch = message.client.commands.get('boxsearchdo');
		
		boxSearch.execute(message,args)

	},
};
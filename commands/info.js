module.exports = {
	name: 'info',
	description: 'Tells you information about the bot.',
	aliases: ['botinfo,information,about,bot'],
	execute(message,args) {
		//gets hoot for messages.
		const hoot = message.client.commands.get('hoot');
		
		var fs = require("fs");
		var stringer = fs.readFileSync('./text/info.txt', { 'encoding': 'utf8'})
		
		hoot.execute(message,stringer);
	},
};